import express, { Request, Response } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";

// Router from express
let helloRouter = express.Router();

// http://localhost:8000/api/hello?name=John
helloRouter.route('/')
    // GET:
    .get(async(req: Request, res: Response) => {
    // Obtanin a Query Param
    let name: any = req?.query?.name;
    LogInfo(`Query Params: ${name}`);
    
    // Call the controller
    const controller: HelloController = new HelloController();
    const response = await controller.getMessage(name);
    return res.send(response);
});

// Export Hello Router
export default helloRouter;