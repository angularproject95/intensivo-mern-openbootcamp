import mongoose from 'mongoose';

export const katasEntity = () => {
    let katasSchema = new mongoose.Schema({
        name: String,
        description: String,
        level: Number,
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users',
            required: true
        },
        date: Date,
        Valoration: {
            type: Number,
            min: 5
        },
        Chances: Number
    });

    return mongoose.model('Katas', katasSchema);
}