import { BasicAndDateResponse } from "./types"
import { IGoodbyeController } from "./interfaces"
import { LogSuccess } from "../utils/logger"

export class GoodbyeController implements IGoodbyeController{
    public async getMessage(name?:string, date?: Date): Promise<BasicAndDateResponse>{
        LogSuccess("[/api/goodbye] Get Request");
        return {
            message: `Goodbye, ${name || "World!"}`,
            date: date || new Date()
        }
    }
}
