/*
*   Basic JSON response for Controller
*/

export type BasicResponse = {
    message: string
}

export type BasicAndDateResponse = {
    message: string,
    date: Date
}

/*
*   Basic JSON response Error for Controller
*/

export type ErrorResponse = {
    error: string,
    message: string
}